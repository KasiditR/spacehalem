using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utiles;

public class ShootRange : MonoBehaviour
{
    [SerializeField] private Transform target;

    void Update()
    {
        Debug.Log(
            $"FOV: {SpaceMath.GetFOV(transform, target)} | DISTANCE: {SpaceMath.Distance(transform.position, target.position)}");
    }
}