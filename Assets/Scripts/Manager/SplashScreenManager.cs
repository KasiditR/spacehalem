﻿using UI;
using UnityEngine;

namespace Manager
{
    public class SplashScreenManager : MonoBehaviour
    {
        [SerializeField] private float fadeLength;
        [SerializeField] private float stayLength;

        private void Awake()
        {
            var fade = fadeLength / 2f;
            var stay = stayLength / 2f;

            FadeScreen.FadeIn(fade, () =>
            {
                FadeScreen.FadeOut(fade, stay,
                    () => { LevelManager.LoadLevel(LevelManager.Type.MainMenu); });
            });
        }
    }
}