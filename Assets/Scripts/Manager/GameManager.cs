﻿using System.Collections;
using Enemy;
using Player;
using UI.Game;
using UnityEngine;
using Utiles;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Transform overrateScene;

        [Header("CHARACTER PREFAB")] [SerializeField]
        private PlayerCharacter playerPrefab;

        [Header("GAME UI")] [SerializeField] private PauseUI pauseUI;
        [SerializeField] private LevelSummaryUI levelSummaryUI;
        [SerializeField] private GameOverUI gameOverUI;
        [SerializeField] private GameUI gameUI;
        [SerializeField] private MapManager mapManager;
        [SerializeField] private AudioManager audioManager;

        [Header("GAMEPLAY SETTINGS")] [SerializeField]
        private int scorePerKill = 10;

        [SerializeField] private int bonusOnLevelCompleted = 100;
        [SerializeField] private int healthOnLevelCompleted = 1;
        private int _cartridgeOnMap;

        private EnemyCharacter[] _enemies;

        private int _enemyCount;
        private PlayerCharacter _player;

        private void Awake()
        {
            _player = Instantiate(playerPrefab);
            levelSummaryUI.Init(this);

            Debug.Assert(pauseUI != null, "pauseUI can't be null!");
            Debug.Assert(levelSummaryUI != null, "levelSummaryUI can't be null!");
            Debug.Assert(gameOverUI != null, "gameOverUI can't be null!");
            Debug.Assert(_player != null, "playerCharacter can't be null!");
            Debug.Assert(mapManager != null, "mapManager can't be null!");

            _player.OnScoreChanged += OnPlayerScoreChanged;
            _player.OnHealthChanged += OnPlayerHealthChanged;
            _player.OnMagazineCountChanged += OnMagazineCountChanged;
            _player.Gun.OnAmmoChanged += OnAmmoChanged;
            _player.Gun.OnShoot += OnShoot;
            foreach (var portal in mapManager.Portals) portal.OnPortalEntered += OnPortalEntered;

            //TODO:
            overrateScene.gameObject.SetActive(false);
        }

        private void Start()
        {
            StartGame();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (pauseUI.IsActive)
                    pauseUI.Hide();
                else
                    pauseUI.Show();
            }
        }

        public void StartGame()
        {
            _player.NextLevel();

            var buildResultData = mapManager.BuildLevel(_player);
            _enemies = buildResultData.Enemies;
            foreach (var enemy in buildResultData.Enemies) enemy.OnDeath += OnEnemyDeath;

            _enemyCount = buildResultData.Enemies.Length;
            _cartridgeOnMap = buildResultData.Cartridge;

            _player.SetHealth(_player.Health); //INITIALIZE SCORE
            _player.AddScore(0); //INITIALIZE SCORE

            gameUI.SetCartridgeCount(_cartridgeOnMap); //INITIALIZE CARTRIDGE ON MAP
            gameUI.SetMagCount(_player.Magazines.Count); //INITIALIZE MAG COUNT
            gameUI.SetAmmoCount(_player.Gun.Ammo); //INITIALIZE AMMO COUNT
            gameUI.SetEnemyCount(_enemyCount); //INITIALIZE ENEMY COUNT

            _player.Show();
        }

        private void OnShoot()
        {
            foreach (var enemy in _enemies)
            {
                if (enemy.IsDead) continue;
                if (SpaceMath.Distance(_player.transform.position, enemy.transform.position) >
                    _player.ShootDistance) continue;
                if (SpaceMath.GetFOV(_player.transform, enemy.transform) > _player.ShootDegree) continue;

                enemy.TakeDamage(_player.Gun.Damage);
            }

            audioManager.Play(audioManager.shoot);
        }

        private void OnAmmoChanged(int ammo)
        {
            gameUI.SetAmmoCount(ammo);
        }

        private void OnMagazineCountChanged(int magCount)
        {
            _cartridgeOnMap--;
            gameUI.SetCartridgeCount(_cartridgeOnMap);
            gameUI.SetMagCount(magCount);
            audioManager.Play(audioManager.reload);
        }

        private void OnEnemyDeath(Character character)
        {
            character.OnDeath -= OnEnemyDeath;

            _enemyCount--;
            gameUI.SetEnemyCount(_enemyCount);
            _player.AddScore(scorePerKill);
            audioManager.Play(audioManager.enemyDead);
        }

        private void OnPlayerScoreChanged(int previousScore, int newScore)
        {
            gameUI.SetScore(newScore);
        }

        private void OnPlayerHealthChanged(int previousHealth, int newHealth)
        {
            gameUI.SetHealth(newHealth);
            if (previousHealth > newHealth) StartCoroutine(SceneDelay());
        }

        private IEnumerator SceneDelay()
        {
            overrateScene.gameObject.SetActive(true);
            audioManager.Play(audioManager.playerHit);
            foreach (var enemy in _enemies)
            {
                if (enemy.IsDead) continue;
                enemy.Hide();
            }

            _player.Hide();

            yield return new WaitForSeconds(5f);

            overrateScene.gameObject.SetActive(false);

            foreach (var enemy in _enemies)
            {
                if (enemy.IsDead) continue;
                enemy.Respawn();
                enemy.Show();
            }

            if (_player.IsDead)
            {
                gameOverUI.SetScore(_player.Score);
                gameOverUI.Show();
            }
            else
            {
                _player.Respawn();
                _player.Show();
            }
        }

        private void OnPortalEntered(Portal portal)
        {
            _player.SetPortalIndex(portal.PortalIndex);
            _player.SetHealth(_player.Health + healthOnLevelCompleted);

            var bonusScore = _enemyCount == 0 ? bonusOnLevelCompleted * _player.Level : 0;
            _player.AddScore(bonusScore);
            levelSummaryUI.SetScore(bonusScore, _player.Score);
            levelSummaryUI.Show();
        }
    }
}