﻿using System;
using Player;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [SerializeField] private int portalIndex;
    public int PortalIndex => portalIndex;
    public Transform SpawnPoint => transform;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<PlayerCharacter>()) return;
        OnPortalEntered?.Invoke(this);
    }

    public event Action<Portal> OnPortalEntered;
}