﻿using Player;
using UnityEngine;

namespace Enemy
{
    public class EnemyCharacter : Character
    {
        [SerializeField] private int attack;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent<IDamagable>(out var damagable)) return;
            damagable.TakeDamage(attack);
        }

        protected override void Dead()
        {
            Destroy(gameObject);
        }
    }
}