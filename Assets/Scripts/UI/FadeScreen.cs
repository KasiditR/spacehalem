﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Canvas), typeof(CanvasScaler), typeof(CanvasGroup))]
    public class FadeScreen : Singleton<FadeScreen>
    {
        private Canvas _canvas;
        private CanvasGroup _canvasGroup;

        public override void Awake()
        {
            base.Awake();
            _canvasGroup = GetComponent<CanvasGroup>();

            _canvas = GetComponent<Canvas>();
            _canvas.renderMode = RenderMode.ScreenSpaceCamera;
            _canvas.worldCamera = Camera.main;

            var canvasScaler = GetComponent<CanvasScaler>();
            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
            canvasScaler.referenceResolution = new Vector2(1920, 1080);

            var overlayImage = new GameObject("OverlayImage").AddComponent<Image>();
            overlayImage.color = Color.black;
            overlayImage.transform.SetParent(transform);

            var rectTransform = GetComponent<RectTransform>();
            rectTransform.anchorMin = new Vector2(1, 0);
            rectTransform.anchorMax = new Vector2(0, 1);
            rectTransform.pivot = new Vector2(0.5f, 0.5f);
        }

        public static void FadeIn(float fadeLength, Action action = null)
        {
            Instance._canvas.worldCamera = Camera.main;
            Instance._canvasGroup.alpha = 1f;
            LeanTween.alphaCanvas(Instance._canvasGroup, 0f, fadeLength).setOnComplete(() => { action?.Invoke(); });
        }

        public static void FadeOut(float fadeLength, float stayLength, Action action = null)
        {
            Instance._canvas.worldCamera = Camera.main;
            Instance._canvasGroup.alpha = 0f;
            LeanTween.value(Instance.gameObject, 0f, 1f, stayLength).setOnComplete(() =>
            {
                LeanTween.alphaCanvas(Instance._canvasGroup, 1f, fadeLength).setOnComplete(() =>
                {
                    action?.Invoke();
                });
            });
        }
    }
}