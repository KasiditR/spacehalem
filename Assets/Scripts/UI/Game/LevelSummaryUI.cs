﻿using Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Game
{
    public class LevelSummaryUI : BaseUI
    {
        [SerializeField] private TextMeshProUGUI summaryText;
        [SerializeField] private Button continueButton;

        public override void Awake()
        {
            base.Awake();

            Debug.Assert(summaryText != null, "summaryText can't be null!");
            Debug.Assert(continueButton != null, "continueButton can't be null!");

            Hide();
        }

        public void Init(GameManager gameManager)
        {
            continueButton.onClick.AddListener(() =>
            {
                Hide();
                gameManager.StartGame();
            });
        }

        public void SetScore(int bonusScore, int totalScore)
        {
            summaryText.text =
                $"SUMMARY\nBonus: +{bonusScore}\nTotal Score: {totalScore}";
        }
    }
}