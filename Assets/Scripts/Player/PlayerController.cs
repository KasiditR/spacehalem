﻿using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float walkSpeed;
        [SerializeField] private float turnSpeed;
        [SerializeField] private float padding;

        private Vector3 _move;
        private float _xBound;
        private float _zBound;

        private void Awake()
        {
            var cam = Camera.main;
            _xBound = cam.ViewportToWorldPoint(new Vector2(1, 0)).x - padding;
            _zBound = cam.ViewportToWorldPoint(new Vector2(0, 1)).z - padding;
        }

        private void Update()
        {
            _move.z = Input.GetAxisRaw("Vertical");
            _move.x = _move.z > 0f ? 0f : Input.GetAxisRaw("Horizontal");
            _move.Normalize();
        }

        private void FixedUpdate()
        {
            var moveDir = transform.forward;
            var velocity = _move.z * walkSpeed * Time.deltaTime;
            var newPosition = transform.position + moveDir * velocity;
            newPosition.x = Mathf.Clamp(newPosition.x, -_xBound, _xBound);
            newPosition.z = Mathf.Clamp(newPosition.z, -_zBound, _zBound);
            transform.position = newPosition;
            transform.Rotate(Vector3.up, _move.x * Time.deltaTime * turnSpeed, Space.World);
        }
    }
}