﻿using UnityEngine;

namespace Utiles

{
    public static class SpaceMath
    {
        public static float GetFOV(Transform from, Transform to)
        {
            var dir = from.forward;
            var dirToTarget = (to.transform.position - from.transform.position).normalized;
            var dot = Dot(dir, dirToTarget);
            return Mathf.Acos(dot) * 180f / Mathf.PI;
        }

        public static float Dot(Vector3 a, Vector3 b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        public static float Distance(Vector3 a, Vector3 b)
        {
            return Mathf.Sqrt(
                (b.x - a.x) * (b.x - a.x) +
                (b.y - a.y) * (b.y - a.y) +
                (b.z - a.z) * (b.z - a.z));
        }
    }
}